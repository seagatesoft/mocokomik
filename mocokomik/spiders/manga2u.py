# -*- coding: utf-8 -*-
from urlparse import urljoin

from scrapy import Request, Spider

from mocokomik.items import MangaItemLoader


class Manga2uMeSpider(Spider):
    name = "manga2u.me"
    allowed_domains = ["manga2u.me"]

    def __init__(self, manga_name, chapter, *args, **kwargs):
        super(Manga2uMeSpider, self).__init__(*args, **kwargs)
        self.manga_name = manga_name
        self.manga_id = manga_name.lower().replace(' ', '_')
        self.chapter = chapter

    def start_requests(self):
        manga_url = 'http://{website}/{manga_id}/{chapter}'.format(
            website=self.name,
            manga_id=self.manga_id,
            chapter=self.chapter
        )
        return [Request(manga_url, callback=self.parse)]

    def parse(self, response):
        mil = MangaItemLoader(response=response)
        mil.add_value('manga_id', self.manga_id)
        mil.add_value('manga_name', self.manga_name)
        mil.add_value('chapter', self.chapter)
        mil.add_css('image_urls', 'img.manga-page::attr(src)')
        mil.add_css('page', 'select.cbo_wpm_pag > option[selected=selected]::text')

        yield mil.load_item()

        current_page = int(mil.get_output_value('page'))
        last_page = int(''.join(
            response.xpath('//select[@class="cbo_wpm_pag"][1]/option[last()]/text()').extract())
        )
        next_urls = response.xpath(
            '//ul[@class="nav_pag"][1]/li/a[contains(., "Next")]/@href'
        ).extract()

        if current_page < last_page and next_urls:
            next_url = urljoin(response.url, next_urls[0])
            yield Request(next_url, callback=self.parse)