# -*- coding: utf-8 -*-

# Scrapy settings for mocokomik project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'mocokomik'

SPIDER_MODULES = ['mocokomik.spiders']
NEWSPIDER_MODULE = 'mocokomik.spiders'
ITEM_PIPELINES = {'mocokomik.pipelines.MocokomikPipeline': 1}
IMAGES_STORE = '/home/seagate/Repositories/mocokomik/komik'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'mocokomik (+http://www.yourdomain.com)'
