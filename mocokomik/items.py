# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy import Field, Item
from scrapy.contrib.loader import ItemLoader
from scrapy.contrib.loader.processor import Identity

from scrapylib.processors import default_input_processor, default_output_processor


class MangaItem(Item):
    manga_id = Field()
    manga_name = Field()
    chapter = Field()
    page = Field()
    image_urls = Field()
    images = Field()


class MangaItemLoader(ItemLoader):
    default_item_class = MangaItem
    default_input_processor = default_input_processor
    default_output_processor = default_output_processor

    image_urls_out = Identity()