# -*- coding: utf-8 -*-
import re

from scrapy import Request
from scrapy.contrib.pipeline.images import ImagesPipeline


class MocokomikPipeline(ImagesPipeline):

    CONVERTED_ORIGINAL = re.compile('^full/[0-9,a-f]+.jpg$')

    def get_media_requests(self, item, info):
        return [
            Request(
                x,
                meta={
                    'image_name': '%s_%s_%s' % (item['manga_id'], item['chapter'], item['page'])
                }
            )

            for x in item.get('image_urls', [])
        ]

    def get_images(self, response, request, info):
        for key, image, buf, in super(MocokomikPipeline, self).get_images(response, request, info):
            if self.CONVERTED_ORIGINAL.match(key):
                key = self.change_filename(key, response)
            yield key, image, buf

    def change_filename(self, key, response):
        return "full/%s.jpg" % response.meta['image_name']
